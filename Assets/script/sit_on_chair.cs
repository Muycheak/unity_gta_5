﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sit_on_chair : MonoBehaviour
{
    bool is_trigger = false;
    public GameObject text;
    public GameObject player;
    public GameObject player_at_chair_animator;
    public float delay_log_new_scene = 2;
    void Start()
    {
        player_at_chair_animator.SetActive(false);
        text.SetActive(false);
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Return) && is_trigger)
        {
            model_static_var_for_spawn.is_open_eye = true;
            StartCoroutine(delay_change_sense());
        }
    }
    IEnumerator delay_change_sense()
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(delay_log_new_scene);
        model_static_var_for_spawn.is_open_eye = false;
        player.SetActive(false);
        player_at_chair_animator.SetActive(true);
    }
    private void OnTriggerEnter(Collider other)
    {
        text.SetActive(true);
        is_trigger = true;
    }
    private void OnTriggerExit(Collider other)
    {
        text.SetActive(false);
        is_trigger = false;
    }
}
